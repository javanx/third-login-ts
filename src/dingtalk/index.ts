let Redis = require('ioredis');
const client = new Redis();
 
const { requestHttp } = require('../request/index')

class dingtalk {
  private authorizeApi = 'https://login.dingtalk.com/oauth2/auth'

  private accessTokenApi = `https://api.dingtalk.com/v1.0/oauth2/userAccessToken`

  private userInfoApi = `https://oapi.dingtalk.com/topapi/v2/user/getuserinfo`

  private option: {
    appId: string;
    appkey: string;
    host: string,
    redirectUrl: string;
  };

  constructor(option: object) {
    this.option = {
      appId: '', // gitee Client ID
      appkey: '', // gitee Client secrets
      host: '', // 开发者服务器域名带http
      redirectUrl: '', // 回调地址，自动拼接host
      ...option
    };
    this.option.redirectUrl = this.option.host + this.option.redirectUrl
  }

  /**
   * 登录跳转到gitee授权，拿回code，并调用回调
   */
  login(res: any) {
    console.log('dingtalk login');
    let redirectUrl = encodeURIComponent(this.option.redirectUrl)
    let path = `${this.authorizeApi}?client_id=${this.option.appId}&redirect_uri=${redirectUrl}&response_type=code&scope=openid&prompt=consent&state=1`
    res.redirect(path);
  }
  /**
   * 回调
   * @param code 回调返回的code
   * @returns 返回登录成功的用户信息
   */
  async callback(code: String){
    const ding_token : String = await client.get('ding_token');
    const is_ding_token : Boolean = await client.get('is_ding_token');

    let url = `${this.accessTokenApi}?code=${code}&clientId=${this.option.appId}&clientSecret=${this.option.appkey}`

    let data = {
      clientId: this.option.appId,
      clientSecret: this.option.appkey,
      code: code,
      grantType: 'authorization_code'
    }
    
    if(is_ding_token && ding_token){
      data.grantType = 'refreshToken'
      data.refreshToken = ding_token
    }
    
    let body = await requestHttp({
      method: 'post',
      url: url,
      body: data
    });

    body = JSON.parse(body);
    
    console.log(url, body)

    await client.set('ding_token', body.refreshToken);
    await client.set('is_ding_token', true);
    client.expire('ding_token', 24 * 60 * 60); // 24小时自动过期

    body = await requestHttp({
      method: 'get',
      url: `${this.userInfoApi}?access_token=${body.accessToken}`
    });

    body = JSON.parse(body);

    return body && body.result || {};
  }
}

module.exports = dingtalk